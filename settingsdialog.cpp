#include "settingsdialog.h"
#include "ui_settingsdialog.h"

#include <QSettings>


SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    loadSettings();

    connect(ui->buttonSave, &QPushButton::clicked, this, &SettingsDialog::saveSettings, Qt::QueuedConnection);
    connect(ui->buttonClose, &QPushButton::clicked, this, &SettingsDialog::loadSettings, Qt::QueuedConnection);
}


SettingsDialog::~SettingsDialog()
{
    delete ui;
}


void SettingsDialog::loadSettings()
{
    QSettings settings;

    settings.beginGroup("voiceprint_maker");
    auto launchCommand = settings.value("launch_command", "").toString();
    this->ui->launchCommandText->setText(launchCommand);
    settings.endGroup();

    settings.beginGroup("database");
    auto databasePath = settings.value("path", "").toString();
    this->ui->databasePathText->setText(databasePath);
    auto updateRate = settings.value("update_rate", "").toDouble();
    this->ui->updateRateText->setText(QString::number(updateRate));
    auto topCount = settings.value("top_count", "").toUInt();
    this->ui->topCountText->setText(QString::number(topCount));
    settings.endGroup();
}


void SettingsDialog::saveSettings()
{
    QSettings settings;
    settings.beginGroup("voiceprint_maker");
    settings.setValue("launch_command", this->ui->launchCommandText->text());
    settings.endGroup();
    settings.beginGroup("database");
    settings.setValue("path", this->ui->databasePathText->text());
    settings.setValue("update_rate", this->ui->updateRateText->text());
    settings.setValue("top_count", this->ui->topCountText->text());
    settings.endGroup();
}
