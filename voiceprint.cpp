#include "voiceprint.h"

#include <QTextStream>


Voiceprint Voiceprint::fromString(QString str)
{
    Voiceprint voiceprint;
    QTextStream in(&str);
    double x;
    QString token;
    bool ok = true;
    while (!in.atEnd())
    {
        in >> token;
        x = token.toDouble(&ok);
        if (ok)
        {
            voiceprint.push_back(x);
        }
    }
    return voiceprint;
}


QString Voiceprint::toString() const
{
    QString str;
    for (auto coord : *this)
    {
        str.append(QString::number(coord, 'g', 10));
        str.append(' ');
    }
    return str;
}
