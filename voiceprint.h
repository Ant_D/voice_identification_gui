#ifndef VOICEPRINT_H
#define VOICEPRINT_H

#include <QString>
#include <QVector>


class Voiceprint : public QVector<double>
{
public:
    static Voiceprint fromString(QString str);

    QString toString() const;
};


#endif // VOICEPRINT_H
