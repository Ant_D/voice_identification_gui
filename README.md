Voiceprint maker GUI implemented using Qt Framework.

This application enables to record voice, create voiceprint, register voiceprint in database and identificate voiceprint owner.

## Main window
![Main window](https://bitbucket.org/Ant_D/voice_identification_gui/raw/master/images/main_window.PNG)

## Voice recording dialog
![Voice recording dialog](https://bitbucket.org/Ant_D/voice_identification_gui/raw/master/images/audio_recorder.PNG)

## Settings dialog
![Settings dialog](https://bitbucket.org/Ant_D/voice_identification_gui/raw/master/images/settings.PNG)

## Identification result
![Identification result](https://bitbucket.org/Ant_D/voice_identification_gui/raw/master/images/result_identification.PNG)

## References
[1] Voiceprint maker repository: https://bitbucket.org/Ant_D/voiceprint_maker