<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AudioRecorder</name>
    <message>
        <location filename="audiorecorder.ui" line="14"/>
        <source>Voice recording</source>
        <translation>Запись голоса</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="24"/>
        <source>Sample rate:</source>
        <translation>Частота дискретизации:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="34"/>
        <source>Audio Codec:</source>
        <translation>Аудио кодек:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="41"/>
        <source>Input Device:</source>
        <translation>Устройство ввода:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="48"/>
        <source>File Container:</source>
        <translation>Медиаконтейнер:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="64"/>
        <source>Channels:</source>
        <translation>Кол-во каналов:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="76"/>
        <source>Encoding Mode:</source>
        <translation>Режим кодирования:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="82"/>
        <source>Constant Quality:</source>
        <translation>Постоянное качество:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="115"/>
        <source>Constant Bitrate:</source>
        <translation>Постоянный битрейт:</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="148"/>
        <source>Output...</source>
        <translation>Вывод...</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="155"/>
        <location filename="audiorecorder.cpp" line="175"/>
        <source>Record</source>
        <translation>Запись</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="165"/>
        <location filename="audiorecorder.cpp" line="168"/>
        <location filename="audiorecorder.cpp" line="176"/>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <location filename="audiorecorder.ui" line="172"/>
        <source>Audio Level:</source>
        <translation>Уровень звука:</translation>
    </message>
    <message>
        <location filename="audiorecorder.cpp" line="136"/>
        <source>Recorded %1 sec</source>
        <translation>Записано %1 секунд</translation>
    </message>
    <message>
        <location filename="audiorecorder.cpp" line="145"/>
        <source>Recording to %1</source>
        <translation>Записывается в %1</translation>
    </message>
    <message>
        <location filename="audiorecorder.cpp" line="149"/>
        <source>Paused</source>
        <translation>Приостановлено</translation>
    </message>
    <message>
        <location filename="audiorecorder.cpp" line="154"/>
        <source>Stopped</source>
        <translation>Остановлено</translation>
    </message>
    <message>
        <location filename="audiorecorder.cpp" line="167"/>
        <location filename="audiorecorder.cpp" line="171"/>
        <source>Stop</source>
        <translation>Стоп</translation>
    </message>
    <message>
        <location filename="audiorecorder.cpp" line="172"/>
        <source>Resume</source>
        <translation>Возобновить</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>Voice Identification</source>
        <translation>Голосовая идентификация</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="36"/>
        <source>Current audiofile:</source>
        <translation>Текущий аудиофайл:</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="56"/>
        <source>Identificate</source>
        <translation>Идентифицировать</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="87"/>
        <source>Record voice</source>
        <translation>Записать голос</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <location filename="mainwindow.cpp" line="20"/>
        <source>Choose audiofile</source>
        <translation>Выбрать аудиофайл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="117"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="128"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="133"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="22"/>
        <source>Audio (*.wav)</source>
        <translation>Аудио (*.wav)</translation>
    </message>
</context>
<context>
    <name>SettingsDialog</name>
    <message>
        <location filename="settingsdialog.ui" line="14"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="20"/>
        <source>Voiceprint maker</source>
        <translation>Создатель голосовых отпечатков</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="29"/>
        <source>Launch command</source>
        <translation>Команда запуска</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="48"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="settingsdialog.ui" line="55"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
</context>
<context>
    <name>T</name>
    <message>
        <location filename="audiorecorder.cpp" line="83"/>
        <location filename="audiorecorder.cpp" line="89"/>
        <location filename="audiorecorder.cpp" line="95"/>
        <location filename="audiorecorder.cpp" line="101"/>
        <location filename="audiorecorder.cpp" line="108"/>
        <location filename="audiorecorder.cpp" line="118"/>
        <source>Default</source>
        <translation>По умолчанию</translation>
    </message>
</context>
<context>
    <name>VoiceprintMaker</name>
    <message>
        <location filename="voiceprintmaker.cpp" line="20"/>
        <source>Failed to launch voiceprint maker</source>
        <translation>Ошибка при запуске создателя голосовых отпечатков</translation>
    </message>
    <message>
        <location filename="voiceprintmaker.cpp" line="23"/>
        <source>Voiceprint maker crashed</source>
        <translation>Создатель голосовых отпечатков аварийно завершился</translation>
    </message>
    <message>
        <location filename="voiceprintmaker.cpp" line="42"/>
        <source>Voiceprint maker finished with code</source>
        <translation>Создатель голосовых отпечатков завершился с кодом</translation>
    </message>
</context>
</TS>
