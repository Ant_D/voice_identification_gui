#ifndef VOICEPRINTMAKER_H
#define VOICEPRINTMAKER_H

#include <QObject>
#include "voiceprint.h"


class QProcess;


class VoiceprintMaker : public QObject
{
    Q_OBJECT
public:
    static VoiceprintMaker *getInstance();

    ~VoiceprintMaker();

    void queryToMake(QString audiofileName);

signals:
    void errorOccured(QString error);
    void made(const Voiceprint &voiceprint);

private:
    static VoiceprintMaker *instance;

    explicit VoiceprintMaker(QObject *parent = nullptr);

    QProcess *process = nullptr;
    QString launchCommand;
    QByteArray response;

    bool updateLaunchCommand();
    void readResponse();
};

#endif // VOICEPRINTMAKER_H
