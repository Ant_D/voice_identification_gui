#include "registrationdialog.h"
#include "ui_registrationdialog.h"

#include "dbmanager.h"
#include "voiceprintmaker.h"


RegistrationDialog::RegistrationDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RegistrationDialog)
{
    ui->setupUi(this);

    connect(ui->nameText, &QLineEdit::textChanged, this, [this](QString text) {
        this->ui->buttonSave->setEnabled(!text.isEmpty());
        QString buttonName = (DbManager::getInstance()->userExists(text) ? tr("Update") : tr("Register"));
        this->ui->buttonSave->setText(buttonName);
    }, Qt::QueuedConnection);
}


RegistrationDialog::~RegistrationDialog()
{
    delete ui;
}


QString RegistrationDialog::getUserName() const
{
    return this->ui->nameText->text();
}
