#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QtWidgets>
#include "audiorecorder.h"
#include "dbmanager.h"
#include "registrationdialog.h"
#include "settingsdialog.h"
#include "voiceprint.h"
#include "voiceprintmaker.h"


Q_DECLARE_METATYPE(Voiceprint)


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
      ui(new Ui::MainWindow),
      audioRecorder(new AudioRecorder(this)),
      registrationDialog(new RegistrationDialog(this)),
      settingsDialog(new SettingsDialog(this))
{
    ui->setupUi(this);

    qRegisterMetaType<Voiceprint>();

    connect(ui->buttonChooseFile, &QPushButton::clicked, this, [this]() {
        QString fileName = QFileDialog::getOpenFileName(
            this,
            tr("Choose audiofile"),
            "",
            tr("Audio (*.wav)")
        );
        if (!fileName.isEmpty())
        {
            this->updateCurrentFile(fileName);
        }
    }, Qt::QueuedConnection);

    connect(ui->currentFileLabel, &QLineEdit::textChanged, this, &MainWindow::updateCurrentFile, Qt::QueuedConnection);

    connect(ui->buttonMakeVoiceprint, &QPushButton::clicked, this, [this]() {
        VoiceprintMaker::getInstance()->queryToMake(this->ui->currentFileLabel->text());
    }, Qt::QueuedConnection);

    connect(ui->actionSettings, &QAction::triggered, this->settingsDialog, &SettingsDialog::open);

    connect(VoiceprintMaker::getInstance(), &VoiceprintMaker::errorOccured, this, &MainWindow::showError, Qt::QueuedConnection);

    connect(VoiceprintMaker::getInstance(), &VoiceprintMaker::made, this, [this](const Voiceprint &voiceprint) {
        QString output = voiceprint.toString();
        this->ui->voiceprintText->setText(output);
    }, Qt::QueuedConnection);

    connect(ui->buttonRecordVoice, &QPushButton::clicked, this->audioRecorder, &AudioRecorder::open, Qt::QueuedConnection);
    connect(audioRecorder, &QDialog::finished, this, [this]() {
        QString outputFile = qobject_cast<AudioRecorder*>(sender())->getOutputLocation();
        if (!outputFile.isEmpty())
        {
            updateCurrentFile(outputFile);
        }
    }, Qt::QueuedConnection);

    connect(DbManager::getInstance(), &DbManager::errorOccured, this, &MainWindow::showError, Qt::QueuedConnection);

    connect(ui->buttonIdentificate, &QPushButton::clicked, ui->buttonMakeVoiceprint, &QPushButton::click, Qt::QueuedConnection);
    connect(ui->buttonIdentificate, &QPushButton::clicked, this, [this]() {
        connect(VoiceprintMaker::getInstance(), &VoiceprintMaker::made, this, &MainWindow::identificate, Qt::QueuedConnection);
    }, Qt::QueuedConnection);
    connect(ui->buttonRegister, &QPushButton::clicked, this->registrationDialog, &QDialog::open, Qt::QueuedConnection);
    connect(this->registrationDialog, &QDialog::accepted, this, [this]() {
        connect(VoiceprintMaker::getInstance(), &VoiceprintMaker::made, this, &MainWindow::addOrUpdateUser, Qt::QueuedConnection);
    }, Qt::QueuedConnection);
    connect(this->registrationDialog, &QDialog::accepted, ui->buttonMakeVoiceprint, &QPushButton::click, Qt::QueuedConnection);
    connect(VoiceprintMaker::getInstance(), &VoiceprintMaker::made, this, [this]() {
        disconnect(VoiceprintMaker::getInstance(), &VoiceprintMaker::made, this, &MainWindow::addOrUpdateUser);
        disconnect(VoiceprintMaker::getInstance(), &VoiceprintMaker::made, this, &MainWindow::identificate);
    }, Qt::QueuedConnection);
}


MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::updateCurrentFile(QString fileName)
{
    this->ui->currentFileLabel->setText(fileName);
    bool isValidFileName = fileName.endsWith(".wav");
    this->ui->buttonMakeVoiceprint->setEnabled(isValidFileName);
    this->ui->buttonIdentificate->setEnabled(isValidFileName);
    this->ui->buttonRegister->setEnabled(isValidFileName);
    this->ui->voiceprintText->clear();
    this->ui->identificationTable->clearContents();
    this->ui->identificationTable->setRowCount(0);
}


void MainWindow::showError(QString error)
{
    QMessageBox(QMessageBox::Critical, "", error).exec();
}


void MainWindow::addOrUpdateUser(const Voiceprint &voiceprint)
{
    QString name = this->registrationDialog->getUserName();
    bool userExists = DbManager::getInstance()->userExists(name);
    if (userExists)
    {
        DbManager::getInstance()->updateUser(name, voiceprint);
    }
    else
    {
        DbManager::getInstance()->addUser(name, voiceprint);
    }
}


void MainWindow::identificate(const Voiceprint &voiceprint)
{
    QVector<QPair<double, QString>> topSimilar = DbManager::getInstance()->findTopSimilarTo(voiceprint);

    qDebug() << "top similar count" << topSimilar.size();
    for (const auto &p : topSimilar)
    {
        qDebug() << p.first << " " << p.second;
    }

    QString name;
    double distance;
    this->ui->identificationTable->setRowCount(topSimilar.size());
    for (int i = 0; i < topSimilar.size(); ++i)
    {
        distance = topSimilar[i].first;
        name = topSimilar[i].second;
        this->ui->identificationTable->setItem(i, 0, new QTableWidgetItem(name));
        this->ui->identificationTable->setItem(i, 1, new QTableWidgetItem(QString::number(distance)));
    }
}
