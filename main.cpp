#include "mainwindow.h"

#include <QApplication>
#include "app_info.h"


void dummyOutput(QtMsgType, const QMessageLogContext&, const QString &) {}


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    app.setOrganizationName(ORGANIZATION_NAME);
    app.setApplicationName(APPLICATION_NAME);

#ifndef QT_DEBUG
    qInstallMessageHandler(dummyOutput);
#endif

    MainWindow w;
    w.show();

    return app.exec();
}
