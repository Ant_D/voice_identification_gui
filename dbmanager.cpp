#include "dbmanager.h"

#include <QSettings>
#include <QtSql>
#include <set>


DbManager *DbManager::instance = nullptr;


DbManager::DbManager(QObject *parent)
    : QObject(parent)
{

}


DbManager *DbManager::getInstance()
{
    if (nullptr == DbManager::instance)
    {
        DbManager::instance = new DbManager(qApp);
    }
    return DbManager::instance;
}


bool DbManager::openDb(const QString &databaseName)
{
    qDebug() << "openDb" << databaseName;
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(databaseName);

    bool opened = db.open();
    if (!opened)
    {
        emit this->errorOccured(tr("Failed to open database"));
        return false;
    }

    QString query = "CREATE TABLE IF NOT EXISTS users "
        "(user_id INTEGER PRIMARY KEY AUTOINCREMENT,"
        "name VARCHAR(64) UNIQUE NOT NULL,"
        "voiceprint TEXT NOT NULL);";

    bool tableIsCreated = QSqlQuery().exec(query);
    if (!tableIsCreated)
    {
        emit this->errorOccured(tr("Failed to create table"));
        return false;
    }

    return true;
}


bool DbManager::reopenDbIfNecessary()
{
    QSettings settings;
    QString dbName = settings.value("database/path", "").toString();
    bool ok = true;
    if (dbName != QSqlDatabase::database().databaseName())
    {
        qDebug() << "reopenDb";
        ok = this->openDb(dbName);
    }
    return ok;
}


void DbManager::addUser(QString name, const Voiceprint &voiceprint)
{
    qDebug() << "add user" << name;
    if (!reopenDbIfNecessary())
    {
        qDebug() << "Failed to open database";
        return;
    }

    QSqlQuery query;
    query.prepare(
        "INSERT INTO users (name, voiceprint)"
        "VALUES(:name, :voiceprint);"
    );
    query.bindValue(":name", name);
    query.bindValue(":voiceprint", voiceprint.toString());
    bool success = query.exec();
    if (!success)
    {
        emit this->errorOccured(query.lastError().text());
    }
}


bool DbManager::userExists(QString name)
{
    qDebug() << "check if user exists" << name;
    if (!reopenDbIfNecessary())
    {
        qDebug() << "Failed to open database";
        return false;
    }

    QSqlQuery query;
    query.prepare("SELECT name FROM users AS U WHERE U.name = :name;");
    query.bindValue(":name", name);

    bool success = query.exec();
    if (!success)
    {
        emit this->errorOccured(query.lastError().text());
    }

    return query.next();
}


Voiceprint DbManager::getVoiceprintByName(QString name)
{
    qDebug() << "get voiceprint by name" << name;
    if (!reopenDbIfNecessary())
    {
        qDebug() << "Failed to open database";
        return {};
    }

    QSqlQuery query;
    query.prepare("SELECT voiceprint FROM users AS U WHERE U.name = :name;");
    query.bindValue(":name", name);

    bool success = query.exec();
    if (!success)
    {
        emit this->errorOccured(query.lastError().text());
    }

    Voiceprint voiceprint;
    if (query.next())
    {
        QString strVoiceprint = query.value(0).toString();
        voiceprint = Voiceprint::fromString(strVoiceprint);
    }

    return voiceprint;
}


void updateVoiceprint(Voiceprint &sourceVoiceprint, const Voiceprint &targetVoiceprint)
{
    QSettings settings;
    double updateRate = settings.value("database/update_rate").toDouble();
    for (int i = 0; i < sourceVoiceprint.size(); ++i)
    {
        sourceVoiceprint[i] += updateRate * (targetVoiceprint[i] - sourceVoiceprint[i]);
    }
}


void DbManager::updateUser(QString name, const Voiceprint &voiceprint)
{
    qDebug() << "update user" << name;
    if (!reopenDbIfNecessary())
    {
        qDebug() << "Failed to open database";
        return;
    }

    Voiceprint dbVoiceprint = DbManager::getInstance()->getVoiceprintByName(name);
    updateVoiceprint(dbVoiceprint, voiceprint);

    QSqlQuery query;
    query.prepare(
        "UPDATE users "
        "SET voiceprint = :voiceprint "
        "WHERE name = :name;"
    );
    query.bindValue(":name", name);
    query.bindValue(":voiceprint", dbVoiceprint.toString());
    bool success = query.exec();
    if (!success)
    {
        emit this->errorOccured(query.lastError().text());
    }
}


double cosineDistance(const Voiceprint &a, const Voiceprint &b)
{
    double dot = 0;
    for (int i = 0; i < a.size(); ++i)
    {
        dot += a[i] * b[i];
    }
    double distance = 0.5 * (1 - dot);
    return distance < 0 ? 0 : distance;
}


QVector<QPair<double, QString>> DbManager::findTopSimilarTo(const Voiceprint &sourceVoiceprint)
{
    qDebug() << "find top similar";
    if (!reopenDbIfNecessary())
    {
        qDebug() << "Failed to open database";
        return {};
    }

    QSqlQuery query;
    bool success = query.exec("SELECT name, voiceprint FROM users;");
    if (!success)
    {
        emit this->errorOccured(query.lastError().text());
    }

    QSqlRecord header = query.record();
    int nameId = header.indexOf("name");
    int voiceprintId = header.indexOf("voiceprint");

    QSettings settings;
    uint topCount = settings.value("database/top_count", "").toUInt();

    Voiceprint voiceprint;
    QString name;
    std::multiset<QPair<double, QString>> sorted;
    while (query.next())
    {
        name = query.value(nameId).toString();
        voiceprint = Voiceprint::fromString(query.value(voiceprintId).toString());
        double distance = cosineDistance(sourceVoiceprint, voiceprint);
        sorted.insert({distance, name});
        while (topCount < sorted.size())
        {
            sorted.erase(--sorted.end());
        }
    }

    QVector<QPair<double, QString>> distanceAndName;
    for (const auto &p : sorted)
    {
        distanceAndName.append(p);
    }

    return distanceAndName;
}
