#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "voiceprint.h"


class AudioRecorder;
class RegistrationDialog;
class SettingsDialog;


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void updateCurrentFile(QString fileName);
    void showError(QString error);
    void addOrUpdateUser(const Voiceprint &voiceprint);
    void identificate(const Voiceprint &voiceprint);

private:
    Ui::MainWindow *ui;
    AudioRecorder *audioRecorder;
    RegistrationDialog *registrationDialog;
    SettingsDialog *settingsDialog;
};
#endif // MAINWINDOW_H
