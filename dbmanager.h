#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QObject>
#include "voiceprint.h"


class DbManager : public QObject
{
    Q_OBJECT
public:
    static DbManager *getInstance();

    void addUser(QString name, const Voiceprint &voiceprint);
    void updateUser(QString name, const Voiceprint &voiceprint);
    bool userExists(QString name);
    QVector<QPair<double, QString>> findTopSimilarTo(const Voiceprint &voiceprint);

signals:
    void errorOccured(QString error);

private:
    static DbManager *instance;

    explicit DbManager(QObject *parent = nullptr);
    bool reopenDbIfNecessary();
    bool openDb(const QString &dbFilePath);
    Voiceprint getVoiceprintByName(QString name);
};

#endif // DBMANAGER_H
