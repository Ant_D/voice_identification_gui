#include "voiceprintmaker.h"

#include <QApplication>
#include <QDataStream>
#include <QDebug>
#include <QProcess>
#include <QSettings>
#include "voiceprint.h"


VoiceprintMaker *VoiceprintMaker::instance = nullptr;


VoiceprintMaker *VoiceprintMaker::getInstance()
{
    if (nullptr == VoiceprintMaker::instance)
    {
        VoiceprintMaker::instance = new VoiceprintMaker(qApp);
    }
    return VoiceprintMaker::instance;
}


VoiceprintMaker::VoiceprintMaker(QObject *parent)
    : QObject(parent),
        process(new QProcess(this))
{
    connect(this->process, &QProcess::readyReadStandardOutput, this, &VoiceprintMaker::readResponse, Qt::QueuedConnection);

    connect(this->process, &QProcess::errorOccurred, this, [this](QProcess::ProcessError error) {
        QString msg = "";
        switch (error)
        {
            case QProcess::FailedToStart:
                msg = tr("Failed to launch voiceprint maker");
            break;
            case QProcess::Crashed:
                msg = tr("Voiceprint maker crashed");
            break;
            default:
                qDebug() << error;
        }
        if ("" != msg)
        {
            emit this->errorOccured(msg);
        }
    }, Qt::QueuedConnection);

    connect(
        this->process,
        QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
        [this](int exitCode, QProcess::ExitStatus exitStatus) {
            qDebug() << "voiceprint maker finished with code" << exitCode << "and with status" << exitStatus;
            if (exitCode)
            {
                emit this->errorOccured(
                    tr("Voiceprint maker finished with code")
                    + ": "
                    + QString::number(exitCode)
                );
            }
        },
        Qt::QueuedConnection
    );

    connect(this->process, &QProcess::readyReadStandardError, this, [this]() {
        qDebug() << QString::fromUtf8(this->process->readAllStandardError()).trimmed();
    }, Qt::QueuedConnection);
}


VoiceprintMaker::~VoiceprintMaker()
{
    if (this->process->state() != QProcess::NotRunning)
    {
        this->process->closeWriteChannel();
        this->process->waitForFinished(-1);
    }
}


void VoiceprintMaker::queryToMake(QString audiofileName)
{
    qDebug() << "queryToMake" << audiofileName;
    qDebug() << this->process->state();

    bool updated = this->updateLaunchCommand();
    bool runningOrStarting = (this->process->state() != QProcess::NotRunning);
    if (updated || !runningOrStarting)
    {
        if (runningOrStarting)
        {
            this->process->closeWriteChannel();
            this->process->waitForFinished(-1);
        }
        this->process->start(this->launchCommand);
        bool started = this->process->waitForStarted(-1);
        qDebug() << "voiceprint maker" << (started ? "started" : "failed to start");
        if (!started)
        {
            return;
        }
    }

    QByteArray request = (audiofileName + "\n").toLatin1();
    this->process->write(request);
    this->response.clear();
}


bool VoiceprintMaker::updateLaunchCommand()
{
    QSettings settings;
    QString cmd = settings.value("voiceprint_maker/launch_command", "").toString();
    bool updated = cmd != this->launchCommand;
    this->launchCommand = cmd;
    return updated;
}


void VoiceprintMaker::readResponse()
{
    qDebug() << "readResponse";
    QString strResponse;
    Voiceprint voiceprint;
    while (0 < this->process->bytesAvailable())
    {
        this->response += this->process->readLine();
        if ('\n' == this->response[this->response.size() - 1])
        {
            strResponse = QString::fromLatin1(this->response);
            voiceprint = Voiceprint::fromString(strResponse);
            this->response.clear();
            emit this->made(voiceprint);
        }
    }
}
